import React from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  MenuItem,
  Popper,
  Grow,
  ClickAwayListener,
  MenuList
} from "@material-ui/core";
import { Paper, Checkbox } from "@material-ui/core";
const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
});
class Jobstic extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstOpen: false,
      secondOpen: false,
      ThirdOpen: false,
      fourthOpen: false,
      firstCheck: [1],
      secondCheck: [2],
      thirdCheck: [3],
      fourthCheck: [4],
      first: null,
      second: null,
      third: null,
      fourth: null
    };
  }
  handleFirstOpen = event => {
    this.setState({
      firstOpen: event.currentTarget,
      first: 1
    });
  };
  handleSecondOpen = event => {
    this.setState({
      secondOpen: event.currentTarget,
      second: 2
    });
  };
  handleThirdOpen = event => {
    this.setState({
      ThirdOpen: event.currentTarget,
      third: 3
    });
  };
  handleFourthOpen = event => {
    this.setState({
      fourthOpen: event.currentTarget,
      fourth: 4
    });
  };

  handleFirstClick = (event, value) => {
    let thirdData = this.state.thirdCheck;
    if (value === 3) {
      if (thirdData.length === 5) {
      } else {
        thirdData.push(...this.state.firstCheck);
        this.setState({
          thirdCheck: thirdData
        });
      }
    }
    if (value === 4) {
      let fourthData = this.state.fourthCheck;
      if (fourthData.length === 5) {
      } else {
        fourthData.push(...this.state.firstCheck);
        this.setState({
          fourthCheck: fourthData
        });
      }
    }
  };
  handleSecondClick = (event, value) => {
    let thirdData = this.state.thirdCheck;
    if (value === 3) {
      if (thirdData.length !== 5) {
        thirdData.push(...this.state.secondCheck);
        this.setState({
          thirdCheck: thirdData
        });
      }
    }
    if (value === 4) {
      let fourthData = this.state.fourthCheck;
      if (fourthData.length === 5) {
      } else {
        fourthData.push(...this.state.secondCheck);
        this.setState({
          fourthCheck: fourthData
        });
      }
    }
  };

  handleThirdClick = (event, value) => {
    // alert("third=" + value);
    let firstData = this.state.firstCheck;
    if (value === 1) {
      if (firstData.length === 5) {
      } else {
        firstData.push(...this.state.thirdCheck);
        this.setState({
          firstCheck: firstData
        });
      }
    }
    if (value === 2) {
      let secondData = this.state.secondCheck;
      if (secondData.length === 5) {
      } else {
        secondData.push(...this.state.thirdCheck);
        this.setState({
          secondCheck: secondData
        });
      }
    }
  };

  handleFourthClick = (event, value) => {
    let firstData = this.state.firstCheck;
    if (value === 1) {
      if (firstData.length === 5) {
      } else {
        firstData.push(...this.state.fourthCheck);
        this.setState({
          firstCheck: firstData
        });
      }
    }
    if (value === 2) {
      let secondData = this.state.secondCheck;
      if (secondData.length === 5) {
      } else {
        secondData.push(...this.state.fourthCheck);
        this.setState({
          secondCheck: secondData
        });
      }
    }
  };

  handleClose = () => {
    this.setState({
      firstOpen: false,
      secondOpen: false,
      ThirdOpen: false,
      fourthOpen: false
    });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <center>
          <Paper
            style={{
              width: "99%",
              height: "99%",
              float: "left",

              marginTop: "10%",
              backgroundColor: "gray"
            }}
          >
            <div>
              <div style={{ marginBottom: "2%" }}>
                <Paper onClick={this.handlePlay}>
                  <center>
                    <h2>Welcome</h2>
                  </center>
                </Paper>
              </div>
              <div style={{ width: "100%" }}>
                <Paper
                  style={{ width: "45%", height: 150, float: "left" }}
                  onClick={e => {
                    e.preventDefault();
                    this.handleFirstOpen(e);
                  }}
                >
                  {this.state.firstCheck.length < 5 ? (
                    this.state.firstCheck.map(checked => (
                      <Checkbox checked={checked} />
                    ))
                  ) : (
                    <h1 style={{ color: "red" }}>Out</h1>
                  )}
                </Paper>
                <Popper
                  open={Boolean(this.state.firstOpen)}
                  anchorEl={this.state.firstOpen}
                  transition
                  disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom"
                      }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handleClose}>
                          <MenuList>
                            <MenuItem
                              onClick={e => {
                                e.preventDefault();
                                this.handleThirdClick(e, 1);
                              }}
                            >
                              Left Hand
                            </MenuItem>
                            <MenuItem
                              onClick={e => {
                                e.preventDefault();
                                this.handleFourthClick(e, 1);
                              }}
                            >
                              Right Hand
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
                <Paper
                  style={{
                    width: "45%",
                    height: 150,
                    float: "right"
                  }}
                  onClick={e => {
                    e.preventDefault();
                    this.handleSecondOpen(e);
                  }}
                >
                  {this.state.secondCheck.length < 5 ? (
                    this.state.secondCheck.map(checked => (
                      <Checkbox checked={checked} />
                    ))
                  ) : (
                    <h1 style={{ color: "red" }}>Out</h1>
                  )}
                </Paper>
                <Popper
                  open={Boolean(this.state.secondOpen)}
                  anchorEl={this.state.secondOpen}
                  transition
                  disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom"
                      }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handleClose}>
                          <MenuList>
                            <MenuItem
                              onClick={e => {
                                e.preventDefault();
                                this.handleThirdClick(e, 2);
                              }}
                            >
                              Left Hand
                            </MenuItem>
                            <MenuItem
                              onClick={e => {
                                e.preventDefault();
                                this.handleFourthClick(e, 2);
                              }}
                            >
                              Right Hand
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </div>
              <div
                style={{
                  width: "100%",
                  height: 150
                }}
              >
                <center>
                  <h1>Welcome..</h1>
                </center>
              </div>

              <div style={{ width: "100%" }}>
                <Paper
                  style={{
                    width: "45%",
                    height: 150,
                    float: "left",
                    marginTop: "41%"
                  }}
                  onClick={e => {
                    e.preventDefault();
                    this.handleThirdOpen(e);
                  }}
                >
                  {this.state.thirdCheck.length < 5 ? (
                    this.state.thirdCheck.map(checked => (
                      <Checkbox checked={checked} />
                    ))
                  ) : (
                    <h1 style={{ color: "red" }}>Out</h1>
                  )}
                </Paper>
                <Popper
                  open={Boolean(this.state.ThirdOpen)}
                  anchorEl={this.state.ThirdOpen}
                  transition
                  disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom"
                      }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handleClose}>
                          <MenuList>
                            <MenuItem
                              onClick={e => {
                                e.preventDefault();
                                this.handleFirstClick(e, 3);
                              }}
                            >
                              Left Hand
                            </MenuItem>
                            <MenuItem
                              onClick={e => {
                                e.preventDefault();
                                this.handleSecondClick(e, 3);
                              }}
                            >
                              Right Hand
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
                <Paper
                  style={{
                    width: "45%",
                    height: 150,
                    marginTop: "41%",
                    float: "right"
                  }}
                  onClick={e => {
                    e.preventDefault();
                    this.handleFourthOpen(e);
                  }}
                >
                  {this.state.fourthCheck.length < 5 ? (
                    this.state.fourthCheck.map(checked => (
                      <Checkbox checked={checked} />
                    ))
                  ) : (
                    <h1 style={{ color: "red" }}>Out</h1>
                  )}
                </Paper>
                <Popper
                  open={Boolean(this.state.fourthOpen)}
                  anchorEl={this.state.fourthOpen}
                  transition
                  disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom"
                      }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handleClose}>
                          <MenuList>
                            <MenuItem
                              onClick={e => {
                                e.preventDefault();
                                this.handleFirstClick(e, 4);
                              }}
                            >
                              Left Hand
                            </MenuItem>
                            <MenuItem
                              onClick={e => {
                                e.preventDefault();
                                this.handleSecondClick(e, 4);
                              }}
                            >
                              Right Hand
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </div>
              <div style={{ marginTop: "80%" }}>
                <Paper onClick={this.handlePlaySelf}>
                  <center>
                    <h2>Self</h2>
                  </center>
                </Paper>
              </div>
            </div>
          </Paper>
        </center>
      </div>
    );
  }
}
export default withStyles(styles)(Jobstic);
