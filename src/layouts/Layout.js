import React from "react";
import { withRouter } from "react-router-dom";

class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      slotsData: []
    };
  }
  render() {
    const { children } = this.props;
    return (
      <div>
        <div>
          <main>{children}</main>
        </div>
      </div>
    );
  }
}
export default withRouter(Layout);
