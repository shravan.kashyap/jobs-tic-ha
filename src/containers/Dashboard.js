import React, { Component } from "react";
import "./Dashboard.css";
import JobsticContainer from "./Jobstic";
import Layout from "../layouts/Layout";
export class DashboardContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }
  render() {
    return (
      <Layout>
        <JobsticContainer />
      </Layout>
    );
  }
}

export default DashboardContainer;
