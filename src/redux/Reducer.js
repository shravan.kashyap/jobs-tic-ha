import {
  SIDEBAR_OPEN,
  FETCH_FEEDS_END,
  MODAL_DATA,
  FETCH_BOARDS_END,
  FETCH_HOTPLATE_END,
  FETCH_FEEDSOURCE_END,
  FETCH_SLOTES_END,
  CREATE_ADD_COURCE,
  CREATE_ADD_BOARD,
  FETCH_FEEDSDATA_END,
  FETCH_FEEDSUPDATE_END
} from "./ActionTypes";

const initialState = {
  sidebarOpen: true,
  modalData: {
    show: false
  }
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SIDEBAR_OPEN:
      return {
        ...state,
        sidebarOpen: !state.sidebarOpen
      };
    case FETCH_FEEDSDATA_END:
      return {
        ...state,
        layData: action.data
      };
    case FETCH_FEEDSUPDATE_END:
      return {
        ...state,
        data1: action.data
      };
    case FETCH_FEEDS_END:
      console.log(action.publisherName);
      if (action.publisherName !== undefined) {
        return {
          ...state,
          data: action.data,
          publisherName: action.publisherName
        };
      } else {
        return {
          ...state,
          data: action.data,
          publisherName: null
        };
      }
    case FETCH_BOARDS_END:
      return {
        ...state,
        boardsData: action.boardsData
      };
    case FETCH_HOTPLATE_END:
      return {
        ...state,
        hotplatesData: action.hotplatesData
      };
    case FETCH_FEEDSOURCE_END:
      return {
        ...state,
        feedSourceData: action.feedSourceData
      };
    case FETCH_SLOTES_END:
      return {
        ...state,
        slotsData: action.slotsData
      };
    case CREATE_ADD_COURCE:
      return {
        ...state,
        FeedSourceData: action.FeedSourceData
      };
    case CREATE_ADD_BOARD:
      return {
        ...state,
        boardsData1: action.boardsData
      };
    case MODAL_DATA:
      return {
        ...state,
        modalData: action.data
      };

    default:
      return state;
  }
};
