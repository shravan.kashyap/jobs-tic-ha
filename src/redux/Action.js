import {
  SIDEBAR_OPEN,
  FETCH_FEEDS_END,
  MODAL_DATA,
  FETCH_BOARDS_END,
  FETCH_HOTPLATE_END,
  FETCH_FEEDSOURCE_END,
  FETCH_SLOTES_END,
  CREATE_ADD_COURCE,
  CREATE_ADD_BOARD,
  FETCH_FEEDSDATA_END,
  CREATE_ADD_SLOTS
} from "./ActionTypes";

import { db } from "../fire/keys";
import moment from "moment";
import sha1 from "sha1";

export const ShowSidebar = () => {
  return {
    type: SIDEBAR_OPEN
  };
};
export const UpdateFeeds = (priorityValue, feedId, data) => {
  return dispatch => {
    db.collection("feeds")
      .doc(feedId)
      .update({ isPriority: priorityValue });
  };
};
export const UpdateFeedsIsRead = feedId => {
  return dispatch => {
    db.collection("feeds")
      .doc(feedId)
      .update({ isRead: true });
  };
};
export const UpdateFeedsBoard = (boardsValue, feedId) => {
  // console.log([boardsValue] !== undefined);
  if ([boardsValue] !== undefined) {
    return dispatch => {
      db.collection("feeds")
        .doc(feedId)
        .update({ boards: boardsValue })
        .then(() => {
          boardsValue.map(board => {
            console.log(board);
            db.collection("boards")
              .doc(board)
              .onSnapshot(data => {
                const boardData = data.data();
                let boardArray1 = boardData.feedId;
                let boardArray = null;
                if (boardArray1 !== "") {
                  boardArray = boardArray1;
                } else {
                  boardArray = [];
                }
                if (boardArray.find(data => data === feedId)) {
                  console.log("Same ID");
                } else {
                  boardArray.push(feedId);
                }
                db.collection("boards")
                  .doc(board)
                  .update({ feedId: boardArray });
              });
          });
        });
    };
  }
};

export const UpdateFeedsHotplates = (hotplateValue, feedId) => {
  return dispatch => {
    db.collection("feeds")
      .doc(feedId)
      .update({
        hotplates: hotplateValue
      })
      .then(() => {
        hotplateValue.map(hotplate => {
          db.collection("hotplates")
            .doc(hotplate)
            .onSnapshot(data => {
              const boardData = data.data();
              let boardArray1 = boardData.feedId;
              let boardArray = null;
              if (boardArray1 !== "") {
                boardArray = boardArray1;
              } else {
                boardArray = [];
              }
              if (boardArray.find(data => data === feedId)) {
                console.log("Same ID");
              } else {
                boardArray.push(feedId);
              }
              db.collection("hotplates")
                .doc(hotplate)
                .update({ feedId: boardArray });
            });
        });
      });
  };
};

export const UpdateFeedsSlots = (slotValue, feedId) => {
  return dispatch => {
    db.collection("feeds")
      .doc(feedId)
      .update({
        slots: slotValue
      })
      .then(() => {
        slotValue.map(slot => {
          db.collection("slots")
            .doc(slot)
            .onSnapshot(data => {
              const boardData = data.data();
              let boardArray1 = boardData.feedId;
              let boardArray = null;
              if (boardArray1 !== "") {
                boardArray = boardArray1;
              } else {
                boardArray = [];
              }
              if (boardArray.find(data => data === feedId)) {
                console.log("Same ID");
              } else {
                boardArray.push(feedId);
              }
              db.collection("slots")
                .doc(slot)
                .update({ feedId: boardArray });
            });
        });
      });
  };
};

export const FetchFeedData = fInstance => {
  return dispatch => {
    let feedsData = [];
    let TodayDate = moment().format("D-M-YYYY");
    db.collection("feeds")
      .where("createdDate", "==", TodayDate)
      .get()
      .then(snapshot => {
        feedsData = [];
        snapshot.forEach(doc => {
          if (doc.exists) {
            feedsData.push(Object.assign({}, doc.data(), { key: doc.id }));
          }
        });
        return dispatch({ type: FETCH_FEEDSDATA_END, data: feedsData });
      });
  };
};
export const FetchFeed = (fInstance, hint) => {
  let TodayDate = moment().format("D-M-YYYY");
  console.log(fInstance + "===" + hint);
  if (fInstance !== undefined) {
    if (hint !== undefined) {
      return dispatch => {
        let feedsData = [];
        let publisherName = "12";
        Promise.all(
          fInstance.map(row => {
            return new Promise((resolve, reject) => {
              db.collection("feeds")
                .doc(row)
                .get()
                .then(snapshot => {
                  feedsData.push(
                    Object.assign({}, snapshot.data(), { key: snapshot.id })
                  );
                })
                .then(resolve)
                .catch(e => reject());
            });
          })
        ).then(() => {
          return dispatch({
            type: FETCH_FEEDS_END,
            data: feedsData,
            publisherName: publisherName
          });
        });
      };
    } else {
      return dispatch => {
        let feedsData = [];
        db.collection("feeds")
          .where("createdDate", "==", TodayDate)
          .where("publisherName", "==", fInstance)
          .get()
          .then(snapshot => {
            feedsData = [];
            snapshot.forEach(doc => {
              if (doc.exists) {
                feedsData.push(Object.assign({}, doc.data(), { key: doc.id }));
              }
            });
            console.log(feedsData);
            return dispatch({
              type: FETCH_FEEDS_END,
              data: feedsData
            });
          });
      };
    }
  } else {
    return dispatch => {
      let feedsData = [];
      db.collection("feeds")
        .where("createdDate", "==", TodayDate)
        .get()
        .then(snapshot => {
          feedsData = [];
          snapshot.forEach(doc => {
            if (doc.exists) {
              feedsData.push(Object.assign({}, doc.data(), { key: doc.id }));
            }
          });

          return dispatch({ type: FETCH_FEEDS_END, data: feedsData });
        });
    };
  }
};
export const FetchBoard = () => {
  return dispatch => {
    let boardsData = [];
    db.collection("boards").onSnapshot(snapshot => {
      boardsData = [];
      snapshot.forEach(doc => {
        if (doc.exists) {
          boardsData.push(Object.assign({}, doc.data(), { key: doc.id }));
        }
      });
      return dispatch({
        type: FETCH_BOARDS_END,
        boardsData: boardsData
      });
    });
  };
};

export const FetchFeedSource = () => {
  return dispatch => {
    let feedSourceData = [];
    db.collection("feedSources")
      .orderBy("organisation")
      .onSnapshot(snapshot => {
        feedSourceData = [];
        snapshot.forEach(doc => {
          if (doc.exists) {
            feedSourceData.push(Object.assign({}, doc.data(), { key: doc.id }));
          }
        });
        return dispatch({
          type: FETCH_FEEDSOURCE_END,
          feedSourceData: feedSourceData
        });
      });
  };
};

export const FetchHotplate = () => {
  return dispatch => {
    let hotplatesData = [];
    db.collection("hotplates")
      .orderBy("name")
      .onSnapshot(snapshot => {
        hotplatesData = [];
        snapshot.forEach(doc => {
          if (doc.exists) {
            hotplatesData.push(Object.assign({}, doc.data(), { key: doc.id }));
          }
        });
        return dispatch({
          type: FETCH_HOTPLATE_END,
          hotplatesData: hotplatesData
        });
      });
  };
};
export const FetchSlots = () => {
  return dispatch => {
    let slotsData = [];
    db.collection("slots")
      .orderBy("name")
      .onSnapshot(snapshot => {
        slotsData = [];
        snapshot.forEach(doc => {
          if (doc.exists) {
            slotsData.push(Object.assign({}, doc.data(), { key: doc.id }));
          }
        });
        return dispatch({
          type: FETCH_SLOTES_END,
          slotsData: slotsData
        });
      });
  };
};
export const modalData = modalData => {
  return {
    type: MODAL_DATA,
    data: modalData
  };
};
// Insert Data Start
export const AddFeedSource = feedSourceData => {
  console.log(feedSourceData);
  const hashedUrl = sha1(feedSourceData.url);
  return dispatch => {
    db.collection("feedSources")
      .doc(hashedUrl)
      .set(
        {
          language: feedSourceData.language,
          organisation: feedSourceData.organisation,
          url: feedSourceData.url
        },
        { merge: true }
      )
      .then(() => {
        alert("source added successfully");
        dispatch({
          type: CREATE_ADD_COURCE,
          feedSourceData
        });
      });
  };
};
export const AddBoard = boardsData => {
  return dispatch => {
    db.collection("boards")
      .add({ feedId: boardsData.feedId, name: boardsData.name })
      .then(() => {
        return dispatch({
          type: CREATE_ADD_BOARD,
          boardsData: boardsData
        });
      });
  };
};
export const AddHotplates = hotplateData => {
  return dispatch => {
    db.collection("hotplates").add({
      feedId: hotplateData.feedId,
      name: hotplateData.name
    });
  };
};
export const AddSlots = slotData => {
  return dispatch => {
    db.collection("slots").add({
      feedId: slotData.feedId,
      name: slotData.name
    });
  };
};
// Insert Data End
