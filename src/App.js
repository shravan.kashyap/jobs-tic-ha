import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import DashboardContainer from "./containers/Dashboard";
import "./App.css";
import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { Provider } from "react-redux";
import { reducer } from "./redux/Reducer";


const store = createStore(reducer, applyMiddleware(thunk));

const App = props => (
  <Provider store={store}>
    <div>
      <Router>
        <div>
          <Route path="/" render={props => <DashboardContainer {...props} />} />
        </div>
        <div />
      </Router>
    </div>
  </Provider>
);

export default App;