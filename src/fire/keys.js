import * as firebase from "firebase";
import "firebase/firestore";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBtGFn-W3RIT7F_2Od6I8z0CTv1LqiqYnY",
  authDomain: "newsfeed-6067c.firebaseapp.com",
  databaseURL: "https://newsfeed-6067c.firebaseio.com",
  projectId: "newsfeed-6067c",
  storageBucket: "newsfeed-6067c.appspot.com",
  messagingSenderId: "176511660172",
  appId: "1:176511660172:web:1b4bcc4717020e76"
};
// var firebaseConfig = {
//   apiKey: "AIzaSyBrXZV0qw83ZUgKVZYsJ38hDvt7cwXVX8c",
//   authDomain: "newsdata-22ab5.firebaseapp.com",
//   databaseURL: "https://newsdata-22ab5.firebaseio.com",
//   projectId: "newsdata-22ab5",
//   storageBucket: "newsdata-22ab5.appspot.com",
//   messagingSenderId: "927306755922",
//   appId: "1:927306755922:web:949e2a5198dd17ed"
// };

firebase.initializeApp(firebaseConfig);

export default firebase;

export const storage = firebase.storage();
export const db = firebase.firestore();
export const auth = firebase.auth();
